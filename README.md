[![pipeline status](https://gitlab.com/smueller18/modbusreader/badges/master/pipeline.svg)](https://gitlab.com/smueller18/modbusreader/commits/master)
[![documentation](https://smueller18.gitlab.io/modbusreader/documentation.svg)](https://smueller18.gitlab.io/modbusreader/)
[![coverage](https://gitlab.com/smueller18/modbusreader/badges/master/coverage.svg)](https://smueller18.gitlab.io/modbusreader/coverage/)
[![pylint](https://smueller18.gitlab.io/modbusreader/lint/pylint.svg)](https://smueller18.gitlab.io/modbusreader/lint/)

# modbusreader
Read values of a modbus server automatically based on a defined schema
