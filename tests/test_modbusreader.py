import unittest
import os
import jsonschema.exceptions
import pymodbus3.exceptions

from modbusreader import ModbusReader

__author__ = "Stephan Müller"
__copyright__ = "2017, Stephan Müller"
__license__ = "MIT"

__dirname__ = os.path.dirname(os.path.abspath(__file__))


class ModbusReaderTests(unittest.TestCase):
    HOST = "localhost"
    PORT = 502
    UNIT = 0

    MODBUS_DEVICE_DEFINITION_EMPTY = {
        "discrete_inputs": {},
        "discrete_outputs": {},
        "input_registers": {},
        "output_registers": {}
    }

    GROUPED_MODBUS_DEVICE_DEFINITION_EMPTY = {
        "discrete_inputs": [],
        "discrete_outputs": [],
        "input_registers": [],
        "output_registers": []
    }

    MODBUS_DEVICE_DEFINITION = {
        "discrete_inputs": {
            "sensor1": {
                "address": 0,
                "description": "test"
            },
            "sensor2": {
                "address": 1,
                "description": "test"
            },
            "sensor3": {
                "address": 4,
                "description": "test"
            }
        },
        "discrete_outputs": {},
        "input_registers": {
            "sensor1": {
                "address": 0,
                "description": "test",
                "type": "int16",
                "unit": "°C",
                "factor": 0.1
            },
            "sensor2": {
                "address": 1,
                "description": "test",
                "type": "uint32",
                "unit": "°C",
                "factor": 0.1
            },
            "sensor3": {
                "address": 4,
                "description": "test",
                "type": "int16",
                "unit": "°C",
                "factor": 0.1
            }
        },
        "output_registers": {}
    }

    GROUPED_MODBUS_DEVICE_DEFINITION = {
        "discrete_inputs": [
            {
                "start_address": 0,
                "count": 2,
                "sensors": {
                    "sensor1": {
                        "address": 0,
                        "description": "test"
                    },
                    "sensor2": {
                        "address": 1,
                        "description": "test"
                    }
                }
            },
            {
                "start_address": 4,
                "count": 1,
                "sensors": {
                    "sensor3": {
                        "address": 4,
                        "description": "test"
                    }
                }
            },
        ],
        "discrete_outputs": [],
        "input_registers": [
            {
                "start_address": 0,
                "count": 3,
                "sensors": {
                    "sensor1": {
                        "address": 0,
                        "description": "test",
                        "type": "int16",
                        "unit": "°C",
                        "factor": 0.1,
                        "count": 1
                    },
                    "sensor2": {
                        "address": 1,
                        "description": "test",
                        "type": "uint32",
                        "unit": "°C",
                        "factor": 0.1,
                        "count": 2
                    }
                }
            },
            {
                "start_address": 4,
                "count": 1,
                "sensors": {
                    "sensor3": {
                        "address": 4,
                        "description": "test",
                        "type": "int16",
                        "unit": "°C",
                        "factor": 0.1,
                        "count": 1
                    }
                }
            },
        ],
        "output_registers": []
    }

    def test_read_all_values(self):
        reader = ModbusReader(self.HOST, self.PORT, self.UNIT, self.MODBUS_DEVICE_DEFINITION)

        with self.assertRaises(pymodbus3.exceptions.ConnectionException):
            reader.read_all_values()

    def test_validate_modbus_schema_definition(self):
        with self.assertRaises(FileNotFoundError):
            ModbusReader(self.HOST, self.PORT, self.UNIT, "")

        with self.assertRaises(jsonschema.exceptions.ValidationError):
            ModbusReader(self.HOST, self.PORT, self.UNIT, dict())

    def test_group_modbus_device_definition(self):
        self.assertDictEqual(
            ModbusReader.group_modbus_device_definition(self.MODBUS_DEVICE_DEFINITION_EMPTY),
            self.GROUPED_MODBUS_DEVICE_DEFINITION_EMPTY)

        with self.assertRaises(KeyError):
            ModbusReader.group_modbus_device_definition(dict())

        self.assertEqual(
                str(ModbusReader.group_modbus_device_definition(self.MODBUS_DEVICE_DEFINITION)),
                str(self.GROUPED_MODBUS_DEVICE_DEFINITION))
